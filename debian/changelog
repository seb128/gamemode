gamemode (1.5.1-1) UNRELEASED; urgency=medium

  * New upstream release
  * Remove README.debian (no longer relevant)
  * Add patch for apparent typo (that doesn't work?   

 -- Jonathan Carter <jcc@debian.org>  Wed, 04 Mar 2020 15:18:36 +0200

gamemode (1.5-1) unstable; urgency=medium

  [ Stephan Lachnit ]
  * New upstream release
  * Update standards version to 4.5
  * Add Rules-Require-Root: no
  * Add debian/clean file and remove debian/.gitignore

  [ Jonathan Carter ]
  * Keep inih subproject until we find a way to use the distro version
  * Update README.debian

 -- Jonathan Carter <jcc@debian.org>  Sun, 23 Feb 2020 10:55:19 +0200

gamemode (1.5~git20190812-107d469-4) unstable; urgency=medium

  * Remove unused lintian override

 -- Jonathan Carter <jcc@debian.org>  Wed, 11 Dec 2019 10:38:11 +0200

gamemode (1.5~git20190812-107d469-3) unstable; urgency=medium

  [ Sebastien Bacher ]
  * debian/gamemode.manpages:
    - remove, the manpage is install by the build system
  * debian/gamemode.install:
    - don't install the service as a systemd system unit, it's an user
      service and correctly installed by the upstream build
    - include the manpage
    - install gamemode-simulate-game in libxec and not /usr/bin, it's a
       debugging utility, not a standard commands to provide to users
   * debian/libgamemode*-dev.install:
     - install the .pc
   * debian/libgamemode0.install:
    - install the cpugovctl and gpuclockctl in /usr/libexec which so
      the service can find them (Closes: #935670)
   * debian/rules:
     - set dh_missing --fail-missing

  [ Jonathan Carter ]
  * Update standards version to 4.4.1
  * Update description for new gamemode-simulate-game path
  * Remove unused lintian overrides

 -- Jonathan Carter <jcc@debian.org>  Wed, 04 Dec 2019 11:55:03 +0000

gamemode (1.5~git20190812-107d469-1) unstable; urgency=medium

  * Upload to unstable

 -- Jonathan Carter <jcc@debian.org>  Mon, 23 Sep 2019 12:11:20 +0000

gamemode (1.5~git20190812-107d469-1~exp2) unstable; urgency=medium

  * Remove unusable symbol files
  * Provide more lintian overrides for missing manpages for trivial programs

 -- Jonathan Carter <jcc@debian.org>  Mon, 23 Sep 2019 10:02:46 +0000

gamemode (1.5~git20190812-107d469-1) experimental; urgency=medium

  * New upstream snapshot
  * debian/control: Add note about gamemode-simulate-game
  * Move gpuclockctl to usr/bin
  * Provide lintian overrides for missing manpages for trivial programs

 -- Jonathan Carter <jcc@debian.org>  Fri, 20 Sep 2019 06:56:38 +0000

gamemode (1.5~git20190722.4ecac89-1) unstable; urgency=medium

  * Bump version number to more accurately reflect version number
    in git
  * Include systemd unit file
  * Add debian/TODO.debian
  * Include default config file
  * Include example to test gamemode as gamemode-simulate-game

 -- Jonathan Carter <jcc@debian.org>  Tue, 06 Aug 2019 07:06:49 +0000

gamemode (1.4+git20190722.4ecac89-2) unstable; urgency=medium

  * Re-enable daemon (previously disabled only for troubleshooting purposes)
    (Closes: #933997)

 -- Jonathan Carter <jcc@debian.org>  Mon, 05 Aug 2019 22:59:40 +0200

gamemode (1.4+git20190722.4ecac89-1) unstable; urgency=medium

  [ Stephan Lachnit ]
  * New upstream release
  * Update standards version to 4.4.0

  [ Jonathan Carter ]
  * New build-dependency: libdbus-1-dev

 -- Stephan Lachnit <stephanlachnit@protonmail.com>  Sun, 21 Jul 2019 15:39:35 +0200

gamemode (1.3.1-1) experimental; urgency=medium

  * New upstream release
  * Update copyright years
  * Update standards version to 4.3.0
  * Upgrade to debhelper-compat (= 12)

 -- Jonathan Carter <jcc@debian.org>  Thu, 05 Apr 2019 16:56:49 +0200

gamemode (1.2-6) unstable; urgency=medium

  * Correct ABI name in -dev packages dependencies
  * Update standards version to 4.2.1

 -- Jonathan Carter <jcc@debian.org>  Wed, 29 Aug 2018 20:18:44 +0200

gamemode (1.2-5) unstable; urgency=medium

  * Add versioned dependency for -dev packates (Closes: #905413)

 -- Jonathan Carter <jcc@debian.org>  Sat, 11 Aug 2018 11:18:56 +0200

gamemode (1.2-4) unstable; urgency=medium

  * Remove debian/gamemode.1 manpage (now provided upstream)
  * Add debian/source directory
  * Add watch file
  * Use upstream manpage
  * Fix copyright for subprojects/inih (Closes: #905552)

 -- Jonathan Carter <jcc@debian.org>  Sun, 05 Aug 2018 17:04:23 +0200

gamemode (1.2-3) unstable; urgency=medium

  * Fix -dev package names and relationships (Closes: #905413)

 -- Jonathan Carter <jcc@debian.org>  Sun, 05 Aug 2018 16:33:16 +0200

gamemode (1.2-2) unstable; urgency=medium

  * Upload to unstable
  * Set architecture to amd64 (only arch supported upstream)
  * Update standards version to 4.2.0

 -- Jonathan Carter <jcc@debian.org>  Fri, 03 Aug 2018 10:32:48 +0200

gamemode (1.2-1) experimental; urgency=medium

  * Initial release (Closes: #898213)

 -- Jonathan Carter <jcc@debian.org>  Fri, 13 Apr 2018 17:35:22 +0200
